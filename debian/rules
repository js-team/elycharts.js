#!/usr/bin/make -f
DEB_DEBIAN_DIR=$(dir $(firstword $(MAKEFILE_LIST)))
DEB_UPSTREAM_VERSION=$(shell dpkg-parsechangelog -l$(DEB_DEBIAN_DIR)/changelog \
                               | sed -rne 's,^Version: ([^+]+)+.*\-.*,\1,p')

%:
	dh $@

override_dh_auto_build:
	mkdir -p build
	cat src/elycharts_defaults.js src/elycharts_core.js src/elycharts_manager_anchor.js \
            src/elycharts_manager_animation.js src/elycharts_manager_highlight.js \
            src/elycharts_manager_label.js src/elycharts_manager_legend.js \
            src/elycharts_manager_mouse.js src/elycharts_manager_tooltip.js \
            src/elycharts_chart_line.js src/elycharts_chart_pie.js > build/elycharts.js
	# Also build the elycharts-full.js, which includes support for balloons, shadows, bar-
	# and funnel-charts.
	cat src/elycharts_defaults.js src/elycharts_core.js src/elycharts_manager_* \
            src/elycharts_chart_* > build/elycharts-full.js

	uglifyjs build/elycharts.js > build/elycharts.min.js
	uglifyjs build/elycharts-full.js > build/elycharts-full.min.js

override_dh_installchangelogs:
	dh_installchangelogs docs/changelog_en.txt

get-orig-source:
	REPACK_TMPDIR=`mktemp -d` && \
	wget -e robots=off https://elycharts.googlecode.com/svn/tags/$(DEB_UPSTREAM_VERSION)/ --mirror \
                --no-parent --exclude-directories="/svn/tags/*/lib","/svn/tags/*/dist" \
                --no-host-directories --cut-dirs=3 --reject="index.html","demo*.html" \
                --directory-prefix=$$REPACK_TMPDIR/elycharts.js-$(DEB_UPSTREAM_VERSION) && \
	GZIP="--best --no-name" tar -C $$REPACK_TMPDIR -czf $$REPACK_TMPDIR/elycharts.js_$(DEB_UPSTREAM_VERSION)+ds.orig.tar.gz elycharts.js-$(DEB_UPSTREAM_VERSION) && \
	rm -r $$REPACK_TMPDIR/elycharts.js-$(DEB_UPSTREAM_VERSION) && \
	mv $$REPACK_TMPDIR/elycharts.js_$(DEB_UPSTREAM_VERSION)+ds.orig.tar.gz $(CURDIR) && \
	rm -rf $$REPACK_TMPDIR
